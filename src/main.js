import { createApp } from 'vue'
import App from './App.vue'



import router from './router';
import ElementPlus from 'element-plus';
import '@/assets/css/common.scss';
import 'element-plus/dist/index.css';
import Antd from 'ant-design-vue'
import 'ant-design-vue/dist/antd.css'
import moment from 'moment'

const app = createApp(App) // 创建实例

app.use(ElementPlus);
app.use(Antd)
app.use(router)
app.config.globalProperties.$moment = moment

app.mount('#app')
