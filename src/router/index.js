import { createRouter, createWebHistory } from "vue-router";

const router = createRouter({
  history: createWebHistory(), // hash模式：createWebHashHistory，history模式：createWebHistory
  routes: [
    {
      path: "/",
      component: () =>
        import(/* webpackChunkName: "address-edit" */ "@/pages/home/index.vue"),
    },
    {
      path: "/fundManagement",
      component: () =>
        import(
          /* webpackChunkName: "address-edit" */ "@/pages/fundManagement/index.vue"
        ),
    },
    {
      path: "/reconciliationCenter",
      component: () =>
        import(
          /* webpackChunkName: "address-edit" */ "@/pages/reconciliationCenter/index.vue"
        ),
    },
  ],
});

export default router;
