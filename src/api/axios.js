import axios from 'axios';

const service = axios.create({
  baseURL: 'http://139.9.46.37:8866',
  time: 15000,
  headers: {
    'Content-Type': 'multipart/form-data'
  }
});

service.interceptors.request.use(
  config => {
    if (config.method === 'get' || config.method === 'put') {
      config.params = config.data;
    }

    const formData = new FormData()
    const data = config.data
    Object.keys(data).forEach((key) => {
      formData.append(key, data[key])
    })
    
    config.data = formData
    return config;
  },
  error => {
    Promise.reject(error);
  }
);

service.interceptors.response.use(
  response => {
    if (response.status !== 200) {
      return Promise.reject('error');
    }
    return response.data
  },
  error => {
    console.log('error', error)
    return Promise.reject(error);
    Promise.reject(error);
  }
);

export default service;
