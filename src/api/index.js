import axios from '@/api/axios';

// 对账中心
export const getListApi = query => {
  return axios({
    method: 'post',
    url: '/shua/app/queryAlipay.php',
    data: query
  });
};

// 首页 资金管理
export const getHomeApi = query => {
  return axios({
    method: 'post',
    url: '/shua/app/queryAlipayIndex.php',
    data: query
  });
};

// 对账中心 对账总览
export const getOverviewApi = query => {
  return axios({
    method: 'post',
    url: '/shua/app/queryAlipayChart.php',
    data: query
  });
};

// 对账中心 下载
export const getDownloadAPi = query => {
  return axios({
    baseURL: 'http://dev.dongtech.net:8866/',
    method: 'get',
    url: '/shua/app/queryAlipayExcelDownload.php',
    data: query
  });
};

